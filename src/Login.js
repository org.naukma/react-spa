import './App.css';
import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { correctName, correctPassword } from './confidential/credentials';

const Login = ({username, password, setUsername, setPassword, onLogin}) => {
    
    const [error, setError] = useState('');
    const navigate = useNavigate();
    
    const isLoginTargeted = username || password;

    const handleLogin = (e) => {
        e.preventDefault();

        onLogin();

        if(username === correctName && password === correctPassword) {
            setError('');
            navigate('/dashboard');
        } else {
            setError('Incorrect login and/or password.')
        }
    };

    return (
        <div className="Login-form">
            <h2>Log to Web App</h2>
            <form onSubmit={handleLogin}>
                {error && <p className='error-message'>{error}</p>}
                <input className="Login-window" type='text' name='name' placeholder='Login' value={username} onChange={(e) => setUsername(e.target.value)}/>
                <input className="Login-window" type='password' name='pass' placeholder='Password' value={password} onChange={(e) => setPassword(e.target.value)}/>
                <input id='Login-button' type='submit' value="Log In" disabled={!isLoginTargeted}/>
            </form>
        </div>
    );
}

export default Login;
